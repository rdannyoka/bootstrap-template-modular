<main id="main">
    <!-- ======= About Section ======= -->
    <?php include 'main/about.php';?>
    <!-- End About Section -->

    <!-- ======= Facts Section ======= -->
    <?php include 'main/facts.php';?>
    <!-- End Facts Section -->

    <!-- ======= Skills Section ======= -->
    <?php include 'main/skills.php';?>
    <!-- End Skills Section -->

    <!-- ======= Resume Section ======= -->
    <?php include 'main/resume.php';?>
    <!-- End Resume Section -->

    <!-- ======= Portfolio Section ======= -->
    <?php include 'main/portofolio.php';?>
    <!-- End Portfolio Section -->

    <!-- ======= Services Section ======= -->
    <?php include 'main/service.php';?>
    <!-- End Services Section -->

    <!-- ======= Testimonials Section ======= -->
    <?php include 'main/testimoni.php';?>
    <!-- End Testimonials Section -->

    <!-- ======= Contact Section ======= -->
    <?php include 'main/contact.php';?>
    <!-- End Contact Section -->
    
</main><!-- End #main -->